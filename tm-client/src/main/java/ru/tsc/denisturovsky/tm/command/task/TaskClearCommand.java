package ru.tsc.denisturovsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.request.TaskClearRequest;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove all tasks";

    @NotNull
    public static final String NAME = "task-clear";

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
