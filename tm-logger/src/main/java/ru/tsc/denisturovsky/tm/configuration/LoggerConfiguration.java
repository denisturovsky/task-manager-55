package ru.tsc.denisturovsky.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.tsc.denisturovsky.tm.api.service.IPropertyService;
import ru.tsc.denisturovsky.tm.service.PropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.tsc.denisturovsky.tm")
public class LoggerConfiguration {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String URL = "tcp://" + propertyService.getJMSServerHost() + ":" + propertyService.getJMSServerPort();

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

}
